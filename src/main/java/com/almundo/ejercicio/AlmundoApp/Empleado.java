package com.almundo.ejercicio.AlmundoApp;

public abstract class Empleado {
	
	private String nombre;
	private Dispatcher dispatcher;
	
	public Empleado(String name, Dispatcher dispatch) {
		this.setNombre(name);
		this.setDispatcher(dispatch);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Dispatcher getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(Dispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	public void atenderLlamada() {
		System.out.println("Hola, usted esta siendo atendido por "+ this);
	}
	
	public void colgarLlamada() {
		System.out.println("Ha sido un placer, hasta luego. "+ this);
		dispatcher.agregarEmpleado(this);
	}
	
	public String toString() {
		return this.getNombre();
	}
	
}
