package com.almundo.ejercicio.AlmundoApp;

import java.util.ArrayList;
import java.util.List;

public class App 
{
    public static void main( String[] args )
    {
        Dispatcher dispatcher = Dispatcher.getInstance();
    	
        List<Llamado> lista = new ArrayList<>();
    	for(int i =0; i<20; i++) {
    		lista.add(new Llamado());
    	}
    	
    	dispatcher.dispatchCall(lista);
    }
}
