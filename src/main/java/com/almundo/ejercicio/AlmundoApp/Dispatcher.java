package com.almundo.ejercicio.AlmundoApp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

public class Dispatcher {

	public static final int CANT_HILOS = 10;
	private static Dispatcher INSTANCE = null;
	private int llamadasProcesadas = 0;
	
	public List<Empleado> listaOperadores = new ArrayList<>();
	public List<Empleado> listaSupervisores = new ArrayList<>();
	public List<Empleado> listaDirectores = new ArrayList<>();
	
	private ExecutorCompletionService<Llamado> executor = null;
	
    private Dispatcher(){
    	listaOperadores.add(new Operador("Operador 1", this));
    	listaOperadores.add(new Operador("Operador 2", this));
    	listaOperadores.add(new Operador("Operador 3", this));
    	listaOperadores.add(new Operador("Operador 4", this));
    	listaOperadores.add(new Operador("Operador 5", this));
//    	listaOperadores.add(new Operador("Operador 6", this));
    	listaSupervisores.add(new Supervisor("Supervisor 1", this));
    	listaSupervisores.add(new Supervisor("Supervisor 2", this));
    	listaDirectores.add(new Director("Director 1", this));
    	listaDirectores.add(new Director("Director 2", this));
    	
    	executor = new ExecutorCompletionService<>(Executors.newFixedThreadPool(CANT_HILOS));
    }

    public static Dispatcher getInstance() {
        if (INSTANCE == null) {
        	INSTANCE = new Dispatcher();
        }
        return INSTANCE;
    }
	
    /**
     * Procesa un listado de llamados de forma concurrente, 
     * Asignando el primer empleado disponible a la llamada entrante.
     * 
     * @param Lista de llamados
     */
	public int dispatchCall(List<Llamado> llamados) {
		
		// creo el listado con los llamados mayores a los procesables.
		List<Llamado> llamadosExcedentes = null;
		if(llamados.size() > CANT_HILOS) {
			llamadosExcedentes = llamados.subList(CANT_HILOS, llamados.size() -1);
		}
		
		int i=0;
		while(i < llamados.size() && i < CANT_HILOS) {
			Llamado llamado = llamados.get(i);
			Empleado empleado = this.proximoEmpleado();
			if(empleado != null) {
				llamado.setEmpleado(empleado);
				executor.submit(llamado);
				llamadasProcesadas++;
				i++;
			}else {
				// Cuando no hay empleados libres espera dentro del while hasta que se desocupe un empleado
				System.out.println("Aguarde y será atendido.");
			}
		}
		
		// proceso los llamados excedentes
		i = 0;
		while(llamadosExcedentes != null && i < llamadosExcedentes.size()) {
			System.out.println("Todas nuestras lineas se encuentran ocupadas. Por favor llame mas tarde.");
			i++;
		}

		return llamadasProcesadas;
	}

	public void agregarEmpleado(Empleado empleado) {
		if(empleado instanceof Operador) {
			listaOperadores.add(empleado);
//			System.out.println("Agrego un operador: " + listaOperadores.toString());
		}
		if(empleado instanceof Supervisor) {
			listaSupervisores.add(empleado);
//			System.out.println("Agrego un operador: " + listaSupervisores.toString());
		}
		if(empleado instanceof Director) {
			listaDirectores.add(empleado);
//			System.out.println("Agrego un operador: " + listaDirectores.toString());
		}
	}

	private Empleado proximoEmpleado() {
		Empleado empleado = null;
		if(!listaOperadores.isEmpty()) {
			empleado = listaOperadores.remove(0);
		}
		else if(!listaSupervisores.isEmpty()) {
			empleado = listaSupervisores.remove(0);
		}
		else if(!listaDirectores.isEmpty()) {
			empleado = listaDirectores.remove(0);
		}
//		System.out.println("Saco un operador: " + empleado);
		return empleado;
	}
}
