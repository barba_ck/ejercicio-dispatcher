package com.almundo.ejercicio.AlmundoApp;

import java.util.concurrent.Callable;

public class Llamado implements Callable<Llamado>{
	
	private Empleado empleado;

	public Llamado() {}
	
	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	@Override
	public Llamado call() throws Exception {
		int duracion = (int) (Math.random() * 6 + 5);
		try {
			empleado.atenderLlamada();
			Thread.sleep(duracion * 1000);
			System.out.println("La llamada ha durado " + duracion + " segundos.");
			empleado.colgarLlamada();
			return this;
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("La llamada ha sido interrumpida");
			return null;
		}
	}
	
}

