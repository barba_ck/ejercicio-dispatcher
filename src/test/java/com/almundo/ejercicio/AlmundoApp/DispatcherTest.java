package com.almundo.ejercicio.AlmundoApp;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class DispatcherTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DispatcherTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DispatcherTest.class );
    }

    /**
     * Testeo una llamada
     */
    public void test1Call()
    {
    	List<Llamado> lista = new ArrayList<>();
		lista.add(new Llamado());
		Dispatcher dispatcher = Dispatcher.getInstance();
        assertTrue( 1 == dispatcher.dispatchCall(lista) );
    }
    
    /**
     * Testeo 10 llamadas
     */
    public void test10Calls()
    {
    	List<Llamado> lista = new ArrayList<>();
    	for(int i =0; i<10; i++) {
    		lista.add(new Llamado());
    	}
    	Dispatcher dispatcher = Dispatcher.getInstance();
        assertTrue( 10 == dispatcher.dispatchCall(lista) );
    }
    
    /**
     * Testeo mas de 10 llamadas
     */
    public void testMoreCalls()
    {
    	List<Llamado> lista = new ArrayList<>();
    	for(int i =0; i<11; i++) {
    		lista.add(new Llamado());
    	}
    	Dispatcher dispatcher = Dispatcher.getInstance();
        assertTrue( 10 == dispatcher.dispatchCall(lista) );
    }
}
